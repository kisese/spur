package com.spurapp.app.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.melnykov.fab.FloatingActionButton;
import com.moxun.tagcloudlib.view.TagCloudView;
import com.spurapp.app.Quote;
import com.spurapp.app.QuoteViewModel;
import com.spurapp.app.R;
import com.spurapp.app.adapter.TextTagsAdapter;
import com.tiancaicc.springfloatingactionmenu.MenuItemView;
import com.tiancaicc.springfloatingactionmenu.OnMenuActionListener;
import com.tiancaicc.springfloatingactionmenu.SpringFloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

public class TagsFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private TagCloudView tagCloudView;
    private View rootView;
    private QuoteViewModel quoteViewModel;
    private ArrayList<String> quoetList;
    private ArrayList<String> authorList;
    FloatingActionButton fab;
    private SpringFloatingActionMenu springFloatingActionMenu;
    private TextTagsAdapter adapter;
    public static TextView quote;
    List<Quote> mainQuoteList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_tags, container, false);
        quote = (TextView) rootView.findViewById(R.id.quote);
        Typeface face = Typeface.createFromAsset(getActivity().getAssets(), "Arizonia-Regular.ttf");
        quote.setTypeface(face);

        quoteViewModel = ViewModelProviders.of(this).get(QuoteViewModel.class);

        final FloatingActionButton fab = new FloatingActionButton(getActivity());
        fab.setType(FloatingActionButton.TYPE_NORMAL);
        fab.setImageResource(R.drawable.ic_action_book);
        fab.setColorPressedResId(R.color.colorPrimary);
        fab.setColorNormalResId(R.color.colorAccent);
        fab.setColorRippleResId(R.color.text_color);
        fab.setShadow(true);

        tagCloudView = (TagCloudView) rootView.findViewById(R.id.tag_cloud);
        adapter = new TextTagsAdapter(getActivity());
        tagCloudView.setAdapter(adapter);

        quoteViewModel.getAllWords().observe(this, new Observer<List<Quote>>() {
            @Override
            public void onChanged(@Nullable final List<Quote> quotes) {
                // Update the cached copy of the words in the adapter.
                Log.e("quotes fragment", quotes.size() + " ");
                quoetList = getQuotes(quotes);
                authorList = getAuthors(quotes);
                adapter.setAuthorsList(loadAuthors(0, 14));
                adapter.setQuotesList(loadQuotes(0, 14));

            }
        });


        springFloatingActionMenu = new SpringFloatingActionMenu.Builder(getActivity())
                .fab(fab)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_star, "1-14", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_clover, "15-29", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_dots, "30-44", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_plant, "45-59", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_butterfly, "60-74", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_crown, "75-89", R.color.icons, this)
                .addMenuItem(R.color.colorAccent, R.drawable.ic_sun, "90-100", R.color.icons, this)
                .animationType(SpringFloatingActionMenu.ANIMATION_TYPE_TUMBLR)
                .revealColor(R.color.colorPrimary)
                .gravity(Gravity.RIGHT | Gravity.BOTTOM)
                .onMenuActionListner(new OnMenuActionListener() {
                    @Override
                    public void onMenuOpen() {
                        fab.setImageResource(R.drawable.ic_action_cancel);

                    }

                    @Override
                    public void onMenuClose() {
                        fab.setImageResource(R.drawable.ic_action_book);
                    }
                })
                .build();

        return rootView;
    }

    public ArrayList<String> getQuotes(List<Quote> quotes) {
        ArrayList<String> quotetList = new ArrayList<String>();

        for (int i = 0; i < quotes.size(); i++) {
            quotetList.add(quotes.get(i).getQuote());
        }
        return quotetList;
    }

    public ArrayList<String> getAuthors(List<Quote> quotes) {
        ArrayList<String> authorList = new ArrayList<String>();
        for (int i = 0; i < quotes.size(); i++) {
            authorList.add(quotes.get(i).getAuthor());
        }
        return authorList;
    }

    public ArrayList<String> loadAuthors(int start, int end) {
        ArrayList<String> resultList = new ArrayList<>();
        for (int i = start; i < end; i++) {
            resultList.add(authorList.get(i));
        }
        return resultList;
    }

    public ArrayList<String> loadQuotes(int start, int end) {
        ArrayList<String> resultList = new ArrayList<>();
        for (int i = start; i < end; i++) {
            resultList.add(quoetList.get(i));
        }
        return resultList;
    }

    @Override
    public void onClick(View view) {
        MenuItemView menuItemView = (MenuItemView) view;
        String label = menuItemView.getLabelTextView().getText().toString();
//        Toast.makeText(getActivity(), menuItemView.getLabelTextView().getText(),Toast.LENGTH_SHORT).show();
//        Snackbar.make(view, label, Snackbar.LENGTH_LONG)
//                .setAction("Action", null).show();

        switch (label) {
            case "1-14":
                adapter.setAuthorsList(loadAuthors(0, 14));
                adapter.setQuotesList(loadQuotes(0, 14));
                springFloatingActionMenu.hideMenu();
                break;
            case "15-29":
                adapter.setAuthorsList(loadAuthors(15, 29));
                adapter.setQuotesList(loadQuotes(15, 29));
                springFloatingActionMenu.hideMenu();
                break;
            case "30-44":
                adapter.setAuthorsList(loadAuthors(30, 44));
                adapter.setQuotesList(loadQuotes(30, 44));
                springFloatingActionMenu.hideMenu();
                break;
            case "45-59":
                adapter.setAuthorsList(loadAuthors(45, 59));
                adapter.setQuotesList(loadQuotes(45, 59));
                springFloatingActionMenu.hideMenu();
                break;
            case "60-74":
                adapter.setAuthorsList(loadAuthors(60, 74));
                adapter.setQuotesList(loadQuotes(60, 74));
                springFloatingActionMenu.hideMenu();
                break;
            case "75-89":
                adapter.setAuthorsList(loadAuthors(75, 89));
                adapter.setQuotesList(loadQuotes(75, 89));
                springFloatingActionMenu.hideMenu();
                break;
            case "90-101":
                adapter.setAuthorsList(loadAuthors(90, 100));
                adapter.setQuotesList(loadQuotes(90, 100));
                springFloatingActionMenu.hideMenu();
                break;
            default:
                adapter.setAuthorsList(loadAuthors(0, 14));
                adapter.setQuotesList(loadQuotes(0, 14));
                springFloatingActionMenu.hideMenu();
                break;
        }
    }

}

package com.spurapp.app;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class QuoteViewModel extends AndroidViewModel {

    private QuoteRepository mRepository;

    private LiveData<List<Quote>> mAllWords;

    public QuoteViewModel(Application application) {
        super(application);
        mRepository = new QuoteRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    public LiveData<List<Quote>> getAllWords() {
        return mAllWords;
    }

    public void insert(Quote word) {
        mRepository.insert(word);
    }
}
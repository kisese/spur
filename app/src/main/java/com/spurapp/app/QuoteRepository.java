package com.spurapp.app;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

public class QuoteRepository {
    private QuoteDao mWordDao;
    private LiveData<List<Quote>> mAllWords;

    QuoteRepository(Application application) {
        QuoteRoomDatabase db = QuoteRoomDatabase.getDatabase(application);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getAllWords();
    }

    LiveData<List<Quote>> getAllWords() {
        return mAllWords;
    }

    public void insert(Quote word) {
        new insertAsyncTask(mWordDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Quote, Void, Void> {

        private QuoteDao mAsyncTaskDao;

        insertAsyncTask(QuoteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Quote... params) {
            Log.e("Insert value", params[0].getAuthor());
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}

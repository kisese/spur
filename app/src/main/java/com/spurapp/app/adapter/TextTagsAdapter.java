package com.spurapp.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marcoscg.dialogsheet.DialogSheet;
import com.moxun.tagcloudlib.view.TagsAdapter;
import com.spurapp.app.R;
import com.spurapp.app.fragment.TagsFragment;
import com.spurapp.app.home.ShareActivity;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by moxun on 16/1/19.
 */
public class TextTagsAdapter extends TagsAdapter {

    private List<String> authorsList = new ArrayList<>();
    private List<String> quotesList = new ArrayList<>();
    private Context context;

    public TextTagsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return authorsList.size();
    }

    @Override
    public View getView(final Context context, final int position, ViewGroup parent) {
        TextView tv = new TextView(context);
        tv.setText(authorsList.get(position));
        tv.setGravity(Gravity.CENTER);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Click", "Tag " + position + " clicked.");


                new DialogSheet(context)
                        .setTitle(authorsList.get(position))
                        .setMessage(quotesList.get(position))
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.ok, new DialogSheet.OnPositiveClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Your action
                                TagsFragment.quote.setText(quotesList.get(position) + "\n" + authorsList.get(position));
                            }
                        })
                        .setNegativeButton("Share", new DialogSheet.OnNegativeClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Your action
                                Intent i = new Intent(context, ShareActivity.class);
                                i.putExtra("quote_text", quotesList.get(position));
                                i.putExtra("quote_author", authorsList.get(position));
                                context.startActivity(i);
                            }
                        })
                        .setBackgroundColor(Color.WHITE) // Your custom background color
                        .setButtonsColorRes(R.color.icons)  // Default color is accent
                        .show();
            }
        });
        tv.setTextColor(Color.WHITE);
        return tv;
    }

    @Override
    public Object getItem(int position) {
        return authorsList.get(position);
    }

    @Override
    public int getPopularity(int position) {
        return position % 7;
    }

    @Override
    public void onThemeColorChanged(View view, int themeColor) {
        view.setBackgroundColor(themeColor);
    }

    public void setAuthorsList(List<String> authorsList) {
        this.authorsList = authorsList;
        notifyDataSetChanged();
    }

    public void setQuotesList(List<String> quotesList) {
        this.quotesList = quotesList;
        notifyDataSetChanged();
    }
}
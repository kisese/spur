package com.spurapp.app.home;

import android.content.Context;

import com.spurapp.app.Quote;
import com.spurapp.app.QuoteViewModel;

/**
 * Created by Brayo on 4/27/2016.
 */
public class DatabaseSeeder {

    private Context context;

    private QuoteViewModel quoteViewModel;

    public DatabaseSeeder(QuoteViewModel quoteViewModel) {
        this.quoteViewModel = quoteViewModel;
    }

    public void seedDB() {
        quoteViewModel.insert(new Quote("Napoleon Hill", "The world has the habit of making room for the man whose words and " +
                "actions show that he knows where he is going"));
        quoteViewModel.insert(new Quote("James Allen", "Circumstance does not make the man; it reveals him to himself"));
        quoteViewModel.insert(new Quote("Michelangelo", "The greater danger for most of us is not that our aim is too high and we miss " +
                "it, but that it is too low and we reach it"));
        quoteViewModel.insert(new Quote("John Kehoe", "Your life is in your hands, to make of it what you choose"));
        quoteViewModel.insert(new Quote("Jim Rohn", "Let others lead small lives, but not you. Let others argue over small things, " +
                "but not you. Let others cry over small hurts, but not you. Let others leave " +
                "their future in someone else's hands, but not you."));
        quoteViewModel.insert(new Quote("Anthony Robbins", "I challenge you to make your life a masterpiece. I challenge you to join the " +
                "ranks of those people who live what they teach, who walk their talk."));
        quoteViewModel.insert(new Quote("Mark Twain", "The secret of getting ahead is getting started"));
        quoteViewModel.insert(new Quote("James Allen", "For true success ask yourself these four questions: Why? Why not? Why not " +
                "me? Why not now?"));
        quoteViewModel.insert(new Quote("Brian Tracy", "Issue a blanket pardon. Forgive everyone who has ever hurt you in any way. " +
                "Forgiveness is a perfectly selfish act. It sets you free from the past"));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "Our greatest glory is not in never failing but in rising up every time we fail"));
        quoteViewModel.insert(new Quote("Earl Nightingale", "Learn to enjoy every minute of your life. Be happy now. Don't wait for " +
                "something outside of yourself to make you happy in the future. Think how " +
                "really precious is the time you have to spend, whether it's at work or with " +
                "your family. Every minute should be enjoyed and savoured."));
        quoteViewModel.insert(new Quote("Theodore Roosevelt", "Far better it is to dare mighty things, to win glorious triumphs, even though " +
                "chequered by failure, than to take rank with those poor souls who neither " +
                "enjoy much nor suffer much, because they live in the grey twilight that " +
                "knows neither victory nor defeat."));
        quoteViewModel.insert(new Quote("Aristotle", "We are what we repeatedly do. Excellence, then, is not an act, but a habit."));
        quoteViewModel.insert(new Quote("Napoleon Bonaparte", "Impossible is a word to be found only in the dictionary of fools."));
        quoteViewModel.insert(new Quote("Mark Twain", "Twenty years from now you will be more disappointed by the things that you " +
                "didn't do than by the ones you did do. So throw off the bowlines. Sail away " +
                "from the safe harbour. Catch the trade winds in your sails. Explore. Dream. " +
                "Discover."));
        quoteViewModel.insert(new Quote("Arthur C. Clarke", "The only way of finding the limits of the possible is by going beyond them " +
                "into the impossible."));
        quoteViewModel.insert(new Quote("Theodore Roosevelt", "It is hard to fail, but it is worse never to have tried to succeed."));
        quoteViewModel.insert(new Quote("Publius Terence", "Fortune favours the brave."));
        quoteViewModel.insert(new Quote("Robert Browning", "Ah, but a man's reach should exceed his grasp, or what's a heaven for?"));
        quoteViewModel.insert(new Quote("Zig Ziglar", "People often say that motivation doesn't last. Well, neither does bathing - " +
                "that's why we recommend it daily."));
        quoteViewModel.insert(new Quote("Napoleon Hill", "Desire is the starting point of all achievement, not a hope, not a wish, but a " +
                "keen pulsating desire, which transcends everything."));
        quoteViewModel.insert(new Quote("Norman Vincent Peale", "People become really quite remarkable when they start thinking that they " +
                "can do things. When they believe in themselves they have the first secret of " +
                "success."));
        quoteViewModel.insert(new Quote("Henry David Thoreau", "Men are born to succeed, not fail."));
        quoteViewModel.insert(new Quote("Anthony Robbins", "What we can or cannot do, what we consider possible or impossible, is " +
                "rarely a function of our true capability. It is more likely a function of our " +
                "beliefs about who we are."));
        quoteViewModel.insert(new Quote("Stephen Covey", "Every human has four endowments- self-awareness, conscience, " +
                "independent will and creative imagination. These give us the ultimate " +
                "human freedom... The power to choose, to respond, to change."));
        quoteViewModel.insert(new Quote("Napoleon Hill", "All the breaks you need in life wait within your imagination, Imagination is " +
                "the workshop of your mind, capable of turning mind energy into " +
                "accomplishment and wealth."));
        quoteViewModel.insert(new Quote("Euripides", "There is just one life for each of us: our own."));
        quoteViewModel.insert(new Quote("Alexander Woollcott", "There is no such thing in anyone's life as an unimportant day."));
        quoteViewModel.insert(new Quote("Brian Tracy", "All successful people men and women are big dreamers. They imagine what " +
                "their future could be, ideal in every respect, and then they work every day " +
                "toward their distant vision, that goal or purpose."));
        quoteViewModel.insert(new Quote("Mark Twain", "The fear of death follows from the fear of life. A man who lives fully is " +
                "prepared to die at any time."));
        quoteViewModel.insert(new Quote("Deepak Chopra", "There are no accidents... there is only some purpose that we haven't yet " +
                "understood."));
        quoteViewModel.insert(new Quote("Virgil", "They can because they think they can."));
        quoteViewModel.insert(new Quote("Jeune.E. McIntyre.", "There are those who dream and wish and there are those who dream and " +
                "work."));
        quoteViewModel.insert(new Quote("William Feather", "No man is a failure who is enjoying life."));
        quoteViewModel.insert(new Quote("Don Miguel Ruiz", "Death is not the biggest fear we have; our biggest fear is taking the risk to " +
                "be alive -- the risk to be alive and express what we really are."));
        quoteViewModel.insert(new Quote("Wayne Dyer", "Be miserable. Or motivate yourself. Whatever has to be done, it's always " +
                "your choice."));
        quoteViewModel.insert(new Quote("Deepak Chopra", "You and I are essentially infinite choice-makers. In every moment of our " +
                "existence, we are in that field of all possibilities where we have access to " +
                "an infinity of choices."));
        quoteViewModel.insert(new Quote("Abraham Lincoln", "You can have anything you want, if you want it badly enough. You can be " +
                "anything you want to be, do anything you set out to accomplish if you hold " +
                "to that desire with singleness of purpose."));
        quoteViewModel.insert(new Quote("Abraham H. Maslow", "If you deliberately plan on being less than you are capable of being, then I " +
                "warn you that you'll be unhappy for the rest of your life."));
        quoteViewModel.insert(new Quote("Dale Carnegie", "Remember, happiness doesn't depend upon who you are or what you have, it " +
                "depends solely upon what you think."));
        quoteViewModel.insert(new Quote("Theodore Roosevelt", "The only man who never makes mistakes is the man who never does " +
                "anything."));
        quoteViewModel.insert(new Quote("Harry Emerson Fosdick", "Self-pity gets you nowhere. One must have the adventurous daring to accept " +
                "oneself as a bundle of possibilities and undertake the most interesting game " +
                "in the world -- making the most of one's best."));
        quoteViewModel.insert(new Quote("Oliver Wendell Holmes, Jr", "Man's mind, once stretched by a new idea, never regains its original " +
                "dimensions."));
        quoteViewModel.insert(new Quote("Socrates", "The nearest way to glory is to strive to be what you wish to be thought to " +
                "be."));
        quoteViewModel.insert(new Quote("Charles Schwab", "The man who trims himself to suit everybody will soon whittle himself away."));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "Do not go where the path may lead, go instead where there is no path and " +
                "leave a trail."));
        quoteViewModel.insert(new Quote("Lee Jampolsky", "At least three times every day take a moment and ask yourself what is really " +
                "important. Have the wisdom and the courage to build your life around your " +
                "answer."));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "To be yourself in a world that is constantly trying to make you something " +
                "else is the greatest accomplishment."));
        quoteViewModel.insert(new Quote("Unknown", "It’s not who you are that holds you back, it’s who you think you’re not."));
        quoteViewModel.insert(new Quote("Bruce Lee", "Simplicity is the key to brilliance."));
        quoteViewModel.insert(new Quote("Federico Fellini", "There is no end. There is no beginning. There is only the infinite passion of " +
                "life."));
        quoteViewModel.insert(new Quote("Og Mandino", "Work as though you would live forever, and live as though you would die " +
                "today."));
        quoteViewModel.insert(new Quote("Napoleon Hill", "What the mind of man can conceive and believe, the mind of man can " +
                "achieve."));
        quoteViewModel.insert(new Quote("Earl of Chesterfield", "Know the true value of time; snatch, seize, and enjoy every moment of it. " +
                "No idleness, no delay, no procrastination; never put off till tomorrow what " +
                "you can do today."));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "Finish each day and be done with it. You have done what you could; some " +
                "blunders and absurdities have crept in; forget them as soon as you can. " +
                "Tomorrow is a new day; you shall begin it serenely and with too high a spirit " +
                "to be encumbered with your old nonsense."));
        quoteViewModel.insert(new Quote("Pope John XXIII", "Consult not your fears but your hopes and your dreams. Think not about " +
                "your frustrations, but about your unfulfilled potential. Concern yourself not " +
                "with what you tried and failed in, but with what it is still possible for you to " +
                "do."));
        quoteViewModel.insert(new Quote("Margaret Thatcher", "Look at a day when you are supremely satisfied at the end. It's not a day " +
                "when you lounge around doing nothing, it's when you've had everything to do " +
                "and you've done it."));
        quoteViewModel.insert(new Quote("Mahatma Gandhi", "You must be the change you wish to see in the world."));
        quoteViewModel.insert(new Quote("Eleanor Roosevelt", "The future belongs to those who believe in the beauty of their dreams."));
        quoteViewModel.insert(new Quote("Deepak Chopra", "Everything that is happening at this moment is a result of the choices you've " +
                "made in the past."));
        quoteViewModel.insert(new Quote("Napoleon Hill", "Cherish your visions and your dreams, as they are the children of your soul, " +
                "the blueprints of your ultimate achievements."));
        quoteViewModel.insert(new Quote("Wayne Dyer", "Successful people make money. It's not that people who make money " +
                "become successful, but that successful people attract money. They bring " +
                "success to what they do."));
        quoteViewModel.insert(new Quote("Henry Ford", "Failure is simply the opportunity to begin again, this time more " +
                "intelligently."));
        quoteViewModel.insert(new Quote("Napoleon Bonaparte", "Victory belongs to the most persevering."));
        quoteViewModel.insert(new Quote("James Allen", "Dream lofty dreams, and as you dream, so you shall become. Your vision is " +
                "the promise of what you shall one day be; your ideal is the prophecy of what " +
                "you shall at last unveil."));
        quoteViewModel.insert(new Quote("Mike Murdock", "You will never possess what you are unwilling to pursue."));
        quoteViewModel.insert(new Quote("Lao-Tzu", "He who controls others may be powerful, but he who has mastered himself " +
                "is mightier still."));
        quoteViewModel.insert(new Quote("Anthony Robbins", "I've come to believe that all my past failure and frustrations were actually " +
                "laying the foundation for the understandings that have created the new " +
                "level of living I now enjoy."));
        quoteViewModel.insert(new Quote("Erica Jong", "If you don't risk anything, then you risk even more."));
        quoteViewModel.insert(new Quote("Zig Ziglar", "It was character that got us out of bed, commitment that moved us into " +
                "action, and discipline that enabled us to follow through."));
        quoteViewModel.insert(new Quote("Buddha", "No matter how hard the past, you can always begin again."));
        quoteViewModel.insert(new Quote("Buddha", "The secret of health for both mind and body is not to mourn for the past, " +
                "not to worry about the future, nor to anticipate troubles, but to live in the " +
                "present moment wisely and earnestly."));
        quoteViewModel.insert(new Quote("Albert Einstein", "There are only two ways to live your life. One is as though nothing is a " +
                "miracle. The other is as if everything is."));
        quoteViewModel.insert(new Quote("Henry David Thoreau", "Go confidently in the direction of your dreams. Live the life you've " +
                "imagined."));
        quoteViewModel.insert(new Quote("T. S. Elliot", "Only those who will risk going too far can possibly find out how far one can " +
                "go."));
        quoteViewModel.insert(new Quote("Buddha", "All that we are is the result of what we have thought."));
        quoteViewModel.insert(new Quote("Seneca", "It is not because things are difficult that we do not dare, it is because we do " +
                "not dare that they are difficult."));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "It was a high counsel that I once heard given to a young person, \"Always do " +
                "what you are afraid to do."));
        quoteViewModel.insert(new Quote("Jim Rohn", "The best motivation is self-motivation. The guy says, “I wish someone " +
                "would come by and turn me on.” What if they don’t show up? You’ve got to " +
                "have a better plan for your life."));
        quoteViewModel.insert(new Quote("Andre Gide", "It is better to be hated for what you are than to be loved for what you are " +
                "not."));
        quoteViewModel.insert(new Quote("Ralph Waldo Emerson", "Our strength grows out of our weakness."));
        quoteViewModel.insert(new Quote("Brian Tracy", "Confidence is a habit that can be developed by acting as if you already had " +
                "the confidence you desire to have."));
        quoteViewModel.insert(new Quote("Eleanor Roosevelt", "You gain strength, courage and confidence by every experience in which you " +
                "really stop to look fear in the face. You must do the thing that you think " +
                "you cannot do."));
        quoteViewModel.insert(new Quote("Chinese Proverb", "If a man does only what is required of him, he is a slave. If a man does more " +
                "than is required of him, he is a free man."));
        quoteViewModel.insert(new Quote("Albert Einstein", "Learn from yesterday, live for today, hope for tomorrow. The important " +
                "thing is to not stop questioning."));
        quoteViewModel.insert(new Quote("Desiderius Erasmus", "The better part of happiness is to wish to be what you are."));
        quoteViewModel.insert(new Quote("Henry Ford", "The whole secret of a successful life is to find out what is one's destiny to " +
                "do, and then do it."));
        quoteViewModel.insert(new Quote("Abraham Lincoln", "Always bear in mind that your own resolution to succeed is more important " +
                "than any one thing."));
        quoteViewModel.insert(new Quote("Albert Einstein", "Imagination is more important than knowledge. Knowledge is limited. " +
                "Imagination encircles the world."));
        quoteViewModel.insert(new Quote("Dale Carnegie", "Are you bored with life? Then throw yourself into some work you believe in " +
                "with all your heart, live for it, die for it, and you will find happiness that " +
                "you had thought could never be yours."));
        quoteViewModel.insert(new Quote("William James", "The greatest discovery of my generation is that a human being can alter his " +
                "life by altering his attitudes of mind."));
        quoteViewModel.insert(new Quote("William Wirt", "Seize the moment of excited curiosity on any subject to solve your doubts; " +
                "for if you let it pass, the desire may never return, and you may remain in " +
                "ignorance."));
        quoteViewModel.insert(new Quote("Buddha", "What you are is what you have been, and what you will be is what you do " +
                "now."));
        quoteViewModel.insert(new Quote("Buddha", "We are what we think. All that we are arises with our thoughts. With our " +
                "thoughts, we make the world."));
        quoteViewModel.insert(new Quote("John Ruskin", "The highest reward for man's toil is not what he gets for it, but what he " +
                "becomes by it."));
        quoteViewModel.insert(new Quote("Josh S. Hinds", "A day will never be anymore than what you make of it."));
        quoteViewModel.insert(new Quote("Basho", "Do not seek to follow in the footsteps of the wise. Seek what they sought."));
        quoteViewModel.insert(new Quote("Bruce Lee", "If you always put limits on everything you do, physical or anything else. It " +
                "will spread into your work and into your life. There are no limits. There are " +
                "only plateaus, and you must not stay there, you must go beyond them."));
        quoteViewModel.insert(new Quote("Roger H. Lincoln", "There are two rules for success: 1) Never tell everything you know…"));
        quoteViewModel.insert(new Quote("Rumi", "You were born with potential. " +
                "You were born with goodness and trust. " +
                "You were born with ideals and dreams. " +
                "You were born with greatness. " +
                "You were born with wings. " +
                "You are not meant for crawling, so don’t. " +
                "You have wings. " +
                "Learn to use them and fly."));
    }
}

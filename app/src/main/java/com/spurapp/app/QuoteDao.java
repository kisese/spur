package com.spurapp.app;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface QuoteDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Quote quote);

    @Query("DELETE FROM quotes_table")
    void deleteAll();

    @Query("SELECT * from quotes_table ORDER BY id ASC")
    LiveData<List<Quote>> getAllWords();
}
